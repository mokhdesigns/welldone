<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Food;

class Category extends Model
{
    protected $guarded = [];


    public function food()
    {
        return $this->hasMany(Food::class);
    }

}
