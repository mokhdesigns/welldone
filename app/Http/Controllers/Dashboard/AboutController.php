<?php

namespace App\Http\Controllers\Dashboard;

use App\About;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AboutController extends Controller
{

    public function index()
    {
        $about = About::take(1)->latest()->get();

        return view('admin.about.index', compact('about'));
    }


    public function edit($id)
    {
        $about = About::findOrFail($id);

        return view('admin.about.edit', compact('about'));
    }


    public function update(Request $request, $id)
    {

        $request->validate([

            'body' => 'required | min: 50'
        ]);

       About::findOrFail($id)->update($request->all());

       session()->flash('message', 'تم    التعديل بنجاح');

      return redirect('dashboard/about');
    }

}
