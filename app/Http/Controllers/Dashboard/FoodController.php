<?php

namespace App\Http\Controllers\Dashboard;

use App\Category;
use App\Food;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FoodController extends Controller
{
    public function index()
    {
        $foods = Food::all();

        return view('admin.food.index', compact('foods'));
    }


    public function create()
    {
        $categories = Category::all();

        return view('admin.food.create', compact('categories'));
    }


    public function store(Request $request)
    {
        $request->validate([

            'name'        => 'required | min:3',
            'body'        => 'required',
            'price'       => 'required',
            'category_id' => 'required',

        ]);

        $input = $request->all();

        Food::create($input);

        session()->flash('message', 'تمت اضافه الطبق بنجاح ');

        return redirect('dashboard/food');
    }



    public function edit($id)
    {
        $food = Food::findOrFail($id);

        return view('admin.food.edit', compact('client'));
    }


    public function update(Request $request, $id)
    {
        $request->validate([

            'name' => 'required | min:3'
        ]);

        $input = $request->all();

        Food::findOrFail($id)->update($input);

        session()->flash('message', 'تمت تعديل الخدمه بنجاح ');

        return redirect('dashboard/food');
    }


    public function destroy($id)
    {
        Food::findOrFail($id)->delete();
        return redirect('dashboard/food');
    }
}
