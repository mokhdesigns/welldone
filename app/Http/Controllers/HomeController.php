<?php

namespace App\Http\Controllers;

use App\About;
use App\Category;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    public function index()
    {
        return view('home');
    }


    public function about()
    {
        $about = About::take(1)->latest()->get();

        return view('website.about', compact('about'));
    }

    public function menu()
    {

        $categories = Category::all();

        return view('website.menu', compact('categories'));
    }

    public function blog()
    {
        return view('website.blog');
    }

    public function contact()
    {
        return view('website.contact');
    }
}
