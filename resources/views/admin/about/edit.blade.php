@extends('layouts.admin')

@section('content')
            <div class="content-page">

                <div class="content">
                    <div class="container-fluid">
                        <div class="page-title-box">
                            <div class="row align-items-center">
                                <div class="col-sm-6">
                                    <h4 class="page-title">  تعديل من نحن</h4>
                                </div>
                                <div class="col-sm-6">
                                </div>
                            </div> <!-- end row -->
                        </div>

<form method="post" action="{{ route('dashboard.about.update', $about->id) }}">
    @csrf
    @method('PUT')

                        <div class="row">
                            <div class="col-12">
                                <div class="card m-b-30">
                                    <div class="card-body">
                                        <div class="form-group form-group-lg row">
                                            <label for="example-text-input" class="col-sm-2 col-form-label">اسم الخدمه</label>
                                            <div class="col-sm-12">
                                                <textarea class="form-control" style="min-height: 333px;" name="body"  id="example-text-input"> {{ $about->body  }}</textarea>
                                            </div>
                                        </div>


                                        <div class="row">
                                            <div class="col-sm-2">
                                            </div>
                                                <div class="col-sm-6 col-md-offset-3">
                                                    <input class="btn-block btn btn-primary" type="submit" value="تعديل النص">
                                                </div>
                                            </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>

                @endsection
