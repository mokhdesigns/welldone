@extends('layouts.admin')

@section('content')
            <div class="content-page">

                <div class="content">
                    <div class="container-fluid">
                        <div class="page-title-box">
                            <div class="row align-items-center">
                                <div class="col-sm-6">
                                    <h4 class="page-title">اضافه خدمه جديد</h4>
                                </div>
                                <div class="col-sm-6">
                                </div>
                            </div> <!-- end row -->
                        </div>

<form method="post" action="{{ route('dashboard.category.store') }}">
    @csrf

                        <div class="row">
                            <div class="col-12">
                                <div class="card m-b-30">
                                    <div class="card-body">
                                        <div class="form-group form-group-lg row">
                                            <label for="example-text-input" class="col-sm-2 col-form-label">اسم القسم</label>
                                            <div class="col-sm-6">
                                                <input class="form-control" name="name" type="text" placeholder="ادخل اسم القسم" id="example-text-input">
                                            </div>
                                        </div>


                                        <div class="row">
                                            <div class="col-sm-2">
                                            </div>
                                                <div class="col-sm-6 col-md-offset-3">
                                                    <input class="btn-block btn btn-primary" type="submit" value="اضف قسم جديد">
                                                </div>
                                            </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>

                @endsection
