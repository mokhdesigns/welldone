@extends('layouts.admin')

@section('content')
            <div class="content-page">

                <div class="content">
                    <div class="container-fluid">
                        <div class="page-title-box">
                            <div class="row align-items-center">
                                <div class="col-sm-6">
                                    <h4 class="page-title">اضافه عميل جديد</h4>
                                </div>
                                <div class="col-sm-6">

                                </div>
                            </div> <!-- end row -->
                        </div>

<form method="post" action="{{ route('dashboard.food.store') }}">
    @csrf

                        <div class="row">
                            <div class="col-12">
                                <div class="card m-b-30">
                                    <div class="card-body">
                                        <div class="form-group form-group-lg row">
                                            <label for="example-text-input" class="col-sm-2 col-form-label">اسم القسم</label>
                                            <div class="col-sm-6">
                                                <select class="form-control" name="category_id"  id="example-text-input">
                                                    @foreach ($categories as $category)

                                                    <option value="{{ $category->id }}"> {{ $category->name }} </option>

                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group form-group-lg row">
                                                <label for="example-text-input" class="col-sm-2 col-form-label">اسم الطبق</label>
                                                <div class="col-sm-6">
                                                    <input class="form-control" name="name" type="text" placeholder="ادخل اسم الطبق" id="example-text-input" required>
                                                </div>
                                            </div>

                                        <div class="form-group form-group-lg row">
                                                <label for="example-text-input" class="col-sm-2 col-form-label"> سعر الطبق </label>
                                                <div class="col-sm-6">
                                                    <input class="form-control" name="price" type="number" placeholder="ادخل سعر الطبق" id="example-text-input" required>
                                                </div>
                                            </div>

                                            <div class="form-group form-group-lg row">
                                                    <label for="example-text-input" class="col-sm-2 col-form-label"> مواصفات الطبق</label>
                                                    <div class="col-sm-6">
                                                        <textarea class="form-control" name="body"  id="example-text-input" required></textarea>
                                                    </div>
                                                </div>

                                        <div class="row">
                                            <div class="col-sm-2">
                                            </div>
                                                <div class="col-sm-6 col-md-offset-3">
                                                    <input class="btn-block btn btn-primary" type="submit" value="اضف طبق جديد">
                                                </div>
                                            </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>

                @endsection
