@extends('layouts.app')

@section('content')



        <!-- Begin page -->
        <div class="accountbg"></div>

        <div class="wrapper-page">
                <div class="card card-pages shadow-none">

                    <div class="card-body">
                        <div class="text-center m-t-0 m-b-15">
                                <a href="index.html" class="logo logo-admin"><img src="{{ asset('images/logo.png') }}" alt="" height="24"></a>
                        </div>
                        <h5 class="font-18 text-center"> قم بتسجيل دخولك.</h5>

                        <form class="form-horizontal m-t-30" action="{{ route('login') }}" method="POST">
                            @csrf

                            <div class="form-group">
                                <div class="col-12">
                                        <label>البريد الالكتروني</label>
                                    <input  type="email" required=""  class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-12">
                                        <label>كلمه المرور</label>
                                    <input  type="password" required="" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-12">
                                    <div class="checkbox checkbox-primary">
                                            <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" id="customCheck1" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                                    <label class="custom-control-label" for="customCheck1"> تذكرني</label>
                                                  </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group text-center m-t-20">
                                <div class="col-12">
                                    <button class="btn btn-primary btn-block btn-lg waves-effect waves-light" type="submit">تسجيل الدخول</button>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        <!-- END wrapper -->



@endsection
