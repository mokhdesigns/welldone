<!DOCTYPE html>
<html lang="en">
<head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <title>{{ config('app.name', 'laravel') }}</title>
        <meta content="Responsive admin theme build on top of Bootstrap 4" name="description" />
        <meta content="Themesdesign" name="author" />
        <link rel="shortcut icon" href="assets/images/favicon.ico">
        <link rel="stylesheet" href="../plugins/morris/morris.css">
        <link href="{{ asset('admin/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('admin/css/metismenu.min.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('admin/css/icons.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('admin/css/style.css') }}" rel="stylesheet" type="text/css">

    </head>

    <body>


        @yield('content')
            <script src="{{ asset('admin/js/jquery.min.js') }}"></script>
            <script src="{{ asset('admin/js/bootstrap.bundle.min.js') }}"></script>
            <script src="{{ asset('admin/js/metismenu.min.js') }}"></script>
            <script src="{{ asset('admin/js/jquery.slimscroll.js') }}"></script>
            <script src="{{ asset('admin/js/waves.min.js') }}"></script>
            <script src="{{ asset('admin/dashboard.init.js') }}"></script>
            <script src="{{ asset('admin/js/app.js') }}"></script>

    </body>
</html>
