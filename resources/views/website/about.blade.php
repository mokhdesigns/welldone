<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>{{ config('app.name')}}</title>

    <!-- mobile setup -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <link rel="stylesheet" href="{{ asset('website/css/style-cocktail.css')}}">
    <meta name="description" content="">
    <meta property="og:description" content="" />
    <link rel="icon" type="image/png" href="{{ asset('images/logo.png') }}">
    <link href="https://fonts.googleapis.com/css?family=Cairo&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css">
</head>

<body>

    <!-- Top shadow -->
    <div class="shadow"></div>
    <!-- end top shadow -->

    <!-- The splash screen -->
    <div id="splash">
        <div class="loader">
            <img class="splash-logo" src="{{ asset('images/logo.png') }}" />
            <div class="line"></div>
        </div>
    </div>
    <!-- End of splash screen -->

    <div id="wrapper">
        <!-- main content -->
        <main>
            <!-- The header for content -->
            <header class="detail">
                <a href="{{ url('/')}}" class="back" data-transition="slide-from-top">
                    <h1>back</h1>
                </a>
                <section>

                </section>
            </header>
            <!-- end header -->
            <div class="content-wrap">
                <div class="content">
                        <i class="far bg fa-heart fa-circle"></i>
                    <section>
                        <header>
                            <h2> من نكون</h2>
                            <h4 class="serif">قصتنا </h4>
                        </header>

                        <section>
                            <p style="text-align: right;line-height: 35px;font-family: 'Cairo', sans-serif;color: orange;font-size: 20px;">
                            @foreach ($about as $item)
                                {{ $item->body }}
                            @endforeach
                            </p>
                        </section>
                    </section>
                </div>
            </div>
            <div data-remodal-id="modal">
                <i class="icon bg icon-CommentwithLines"></i>
                <button data-remodal-action="close" class="remodal-close"></button>
                <h1></h1>
                <p></p>
                <div class="signature center">
                    <h6>-CHEF-</h6>
                    <h5>Pierre Gabant</h5>
                </div>
            </div>
        </main>
        <!-- end of main content -->
    </div>

    <!-- The slideshow -->
    <ul id="slideshow" data-speed="6000">
        <li>
            <img src="{{ asset('images/1.jpg') }}" alt="slideshow image" />
        </li>
        <li>
            <img src="{{ asset('images/1.jpg') }}" alt="slideshow image" />
        </li>
        <li>
            <img src="{{ asset('images/1.jpg') }}" alt="slideshow image" />
        </li>
        <li>
            <img src="{{ asset('images/1.jpg') }}" alt="slideshow image" />
        </li>
        <li>
            <img src="{{ asset('images/1.jpg') }}" alt="slideshow image" />
        </li>
        <li>
            <img src="{{ asset('images/1.jpg') }}" alt="slideshow image" />
        </li>
        <li>
            <img src="{{ asset('images/1.jpg') }}" alt="slideshow image" />
        </li>
        <li>
            <img src="{{ asset('images/1.jpg') }}" alt="slideshow image" />
        </li>
        <li>
            <img src="{{ asset('images/1.jpg') }}" alt="slideshow image" />
        </li>
    </ul>
    <!-- end of slideshow -->

    <!-- Root element of PhotoSwipe. Must have class pswp. -->
    <div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">

        <!-- Background of PhotoSwipe.
         It's a separate element as animating opacity is faster than rgba(). -->
        <div class="pswp__bg"></div>

        <!-- Slides wrapper with overflow:hidden. -->
        <div class="pswp__scroll-wrap">

            <!-- Container that holds slides.
            PhotoSwipe keeps only 3 of them in the DOM to save memory.
            Don't modify these 3 pswp__item elements, data is added later on. -->
            <div class="pswp__container">
                <div class="pswp__item"></div>
                <div class="pswp__item"></div>
                <div class="pswp__item"></div>
            </div>

            <!-- Default (PhotoSwipeUI_Default) interface on top of sliding area. Can be changed. -->
            <div class="pswp__ui pswp__ui--hidden">

                <div class="pswp__top-bar">

                    <!--  Controls are self-explanatory. Order can be changed. -->

                    <div class="pswp__counter"></div>

                    <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>

                    <button class="pswp__button pswp__button--share" title="Share"></button>

                    <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>

                    <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>

                    <div class="pswp__preloader">
                        <div class="pswp__preloader__icn">
                            <div class="pswp__preloader__cut">
                                <div class="pswp__preloader__donut"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                    <div class="pswp__share-tooltip"></div>
                </div>

                <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
            </button>

                <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
            </button>

                <div class="pswp__caption">
                    <div class="pswp__caption__center"></div>
                </div>

            </div>

        </div>

    </div>
    <script type="text/javascript" src="{{ asset('website/js/pack.js') }}"></script>
    <script type="application/javascript">
        $(document).ready(function() {
            var currentUrl = window.location.href;

            $('body').on('pageActivated', function() {

            });
        });
    </script>

</body>

</html>
