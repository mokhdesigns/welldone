<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>{{ config('app.name')}}</title>

    <!-- mobile setup -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <link rel="stylesheet" href="{{ asset('website/css/style-cocktail.css')}}">
    <meta name="description" content="">
    <meta property="og:description" content="" />
    <link rel="icon" type="image/png" href="{{ asset('images/logo.png') }}">
    <link href="https://fonts.googleapis.com/css?family=Cairo|Reem+Kufi&display=swap" rel="stylesheet">
</head>

<body>

    <!-- Top shadow -->
    <div class="shadow"></div>
    <!-- end top shadow -->

    <!-- The splash screen -->
    <div id="splash">
        <div class="loader">
            <img class="splash-logo" src="{{ asset('images/logo.png') }}" />
            <div class="line"></div>
        </div>
    </div>

    <div id="wrapper">
        <main>
            <header class="detail">
                <a href="{{ url('/') }}" class="back" data-transition="slide-from-top">
                    <h1>back</h1>
                </a>
                <section>
                    <h3 class="badge">blog</h3>
                    <h1>Stories &amp; Taste</h1>
                </section>
            </header>
            <!-- end header -->
            <div class="content-wrap">
                <div class="content">
                    <i class="icon bg icon-CommentwithLines"></i>
                    <section>
                        <header>
                            <h2>We love herbs!</h2>
                            <h4 class="serif">• 10 December •</h4>
                        </header>

                        <p>Curabitur blandit tempus porttitor. Nullam id dolor id nibh ultricies vehicula ut id elit. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Donec id elit non mi porta gravida at eget
                            metus. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>

                        <figure>

                            <a href="https://ambiance.vagebond.nl/html/template/assets/img/slideshow/new/1.jpg" class="gallery-item">
                                <img src="https://ambiance.vagebond.nl/html/template/assets/img/slideshow/new/1.jpg" alt="Fabulous breakfast" />

                                <figcaption>
                                    <h3>Fabulous breakfast</h3>
                                    <p class="serif">Something different</p>
                                    <i class="icon icon-Plus"></i>
                                </figcaption>
                            </a>
                        </figure>
                        <blockquote>
                            <p>"It&#039;s like tasting a slice of heaven"</p>
                        </blockquote>

                        <p>Donec sed odio dui. Curabitur blandit tempus porttitor. Donec ullamcorper nulla non metus auctor fringilla. Morbi leo risus, porta ac consectetur ac, vestibulum at eros.</p>

                        <figure>

                            <a href="https://ambiance.vagebond.nl/html/template/assets/img/slideshow/new/2.jpg" class="gallery-item">
                                <img src="https://ambiance.vagebond.nl/html/template/assets/img/slideshow/new/2.jpg" alt="Something different" />

                                <figcaption>
                                    <h3>Something different</h3>
                                    <p class="serif">5 star presentation</p>
                                    <i class="icon icon-Plus"></i>
                                </figcaption>
                            </a>
                        </figure>
                        <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Duis mollis, est non commodo luctus. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec ullamcorper nulla non metus auctor fringilla.</p>

                        <figure>

                            <a href="https://ambiance.vagebond.nl/html/template/assets/img/slideshow/new/3.jpg" class="gallery-item">
                                <img src="https://ambiance.vagebond.nl/html/template/assets/img/slideshow/new/3.jpg" alt="Noodles with sesame" />

                                <figcaption>
                                    <h3>Noodles with sesame</h3>
                                    <p class="serif">Soulfood for the soul</p>
                                    <i class="icon icon-Plus"></i>
                                </figcaption>
                            </a>
                        </figure>
                        <ul class="simple-list">
                            <li>
                                <h4>taste is personal</h4>
                            </li>
                            <li>
                                <h4>It will always be like that</h4>
                            </li>
                            <li>
                                <h4>Some have it, some don&#039;t</h4>
                            </li>
                        </ul>
                        <p>Sed posuere consectetur est at lobortis. Vestibulum id ligula porta felis euismod semper. Nullam quis risus eget urna mollis ornare vel eu leo.</p>
                    </section>
                </div>
            </div>
            <!-- Block footer with signature and share links -->
            <footer class="overlay-dark">
                <div class="content">
                    <div class="signature">
                        <h6>Written by.</h6>
                        <h5>Pierre Gabant</h5>
                    </div>

                    <section class="share">
                        <h5>Share</h5>
                        <ul>
                            <li>
                                <a href="/" class="instagram">
                                    <i class="icon-social instagram"></i>
                                </a>
                            </li>
                            <li>
                                <a href="/" class="dribbble">
                                    <i class="icon-social dribbble"></i>
                                </a>
                            </li>
                            <li>
                                <a href="/" class="Pinterest">
                                    <i class="icon-social pinterest"></i>
                                </a>
                            </li>
                        </ul>
                    </section>
                </div>
            </footer>
            <!-- end footer -->
            <ul class="link-list">
                <li>
                    <a href="https://ambiance.vagebond.nl/html/template/blog/we-like-it-fresh" data-transition="slide-left">
                        <div class="content">
                            <h3>We like it fresh!</h3>
                            <ul>
                                <li>
                                    <i class="icon icon-Calendar"></i>
                                    <p>10 December</p>
                                </li>
                                <li>
                                    <i class="icon icon-Clock"></i>
                                    <p>0 min read</p>
                                </li>
                            </ul>
                            <i class="next icon icon-RightArrow"></i>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="https://ambiance.vagebond.nl/html/template/blog/weekly-special" data-transition="slide-left">
                        <div class="content">
                            <h3>Weekly special</h3>
                            <ul>
                                <li>
                                    <i class="icon icon-Calendar"></i>
                                    <p>10 December</p>
                                </li>
                                <li>
                                    <i class="icon icon-Clock"></i>
                                    <p>1 min read</p>
                                </li>
                            </ul>
                            <i class="next icon icon-RightArrow"></i>
                        </div>
                    </a>
                </li>
            </ul>
            <div data-remodal-id="modal">
                <i class="icon bg icon-CommentwithLines"></i>
                <button data-remodal-action="close" class="remodal-close"></button>
                <h1></h1>
                <p></p>
                <div class="signature center">
                    <h6>-CHEF-</h6>
                    <h5>Pierre Gabant</h5>
                </div>
            </div>
        </main>
        <!-- end of main content -->
    </div>

    <!-- The slideshow -->
    <ul id="slideshow" data-speed="6000">
        <li>
            <img src="{{ asset('images/1.jpg') }}" alt="slideshow image" />
        </li>
        <li>
            <img src="{{ asset('images/1.jpg') }}" alt="slideshow image" />
        </li>
        <li>
            <img src="{{ asset('images/1.jpg') }}" alt="slideshow image" />
        </li>
        <li>
            <img src="{{ asset('images/1.jpg') }}" alt="slideshow image" />
        </li>
        <li>
            <img src="{{ asset('images/1.jpg') }}" alt="slideshow image" />
        </li>
        <li>
            <img src="{{ asset('images/1.jpg') }}" alt="slideshow image" />
        </li>
        <li>
            <img src="{{ asset('images/1.jpg') }}" alt="slideshow image" />
        </li>
        <li>
            <img src="{{ asset('images/1.jpg') }}" alt="slideshow image" />
        </li>
        <li>
            <img src="{{ asset('images/1.jpg') }}" alt="slideshow image" />
        </li>
    </ul>
    <!-- end of slideshow -->

    <!-- Root element of PhotoSwipe. Must have class pswp. -->
    <div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">

        <!-- Background of PhotoSwipe.
         It's a separate element as animating opacity is faster than rgba(). -->
        <div class="pswp__bg"></div>

        <!-- Slides wrapper with overflow:hidden. -->
        <div class="pswp__scroll-wrap">

            <!-- Container that holds slides.
            PhotoSwipe keeps only 3 of them in the DOM to save memory.
            Don't modify these 3 pswp__item elements, data is added later on. -->
            <div class="pswp__container">
                <div class="pswp__item"></div>
                <div class="pswp__item"></div>
                <div class="pswp__item"></div>
            </div>

            <!-- Default (PhotoSwipeUI_Default) interface on top of sliding area. Can be changed. -->
            <div class="pswp__ui pswp__ui--hidden">

                <div class="pswp__top-bar">

                    <!--  Controls are self-explanatory. Order can be changed. -->

                    <div class="pswp__counter"></div>

                    <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>

                    <button class="pswp__button pswp__button--share" title="Share"></button>

                    <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>

                    <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>

                    <!-- Preloader demo http://codepen.io/dimsemenov/pen/yyBWoR -->
                    <!-- element will get class pswp__preloader--active when preloader is running -->
                    <div class="pswp__preloader">
                        <div class="pswp__preloader__icn">
                            <div class="pswp__preloader__cut">
                                <div class="pswp__preloader__donut"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                    <div class="pswp__share-tooltip"></div>
                </div>

                <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
            </button>

                <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
            </button>

                <div class="pswp__caption">
                    <div class="pswp__caption__center"></div>
                </div>

            </div>

        </div>

    </div>
    <!-- scripts -->
    <script type="text/javascript" src="https://ambiance.vagebond.nl/html/template/assets/js/pack.js"></script>
    <!-- end of scripts -->

    <script type="application/javascript">
        $(document).ready(function() {
            var currentUrl = window.location.href;

            $('body').on('pageActivated', function() {
                $('.back', '#wrapper').attr('href', currentUrl);
            });
        });
    </script>

    <script>
        (function(i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function() {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-72227788-1', 'auto');
        ga('send', 'pageview');
    </script>
</body>

</html>
