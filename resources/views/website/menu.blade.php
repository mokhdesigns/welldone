<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>{{ config('app.name')}}</title>

    <!-- mobile setup -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <link rel="stylesheet" href="{{ asset('website/css/style-cocktail.css')}}">
    <meta name="description" content="">
    <meta property="og:description" content="" />
    <link rel="icon" type="image/png" href="{{ asset('images/logo.png') }}">
    <link href="https://fonts.googleapis.com/css?family=Cairo|Reem+Kufi&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">

    <style>

        .taps{
            transform: scale(0);

        }

        .active-pan{
            transform: scale(1);
        }
    </style>
</head>

<body>

    <!-- Top shadow -->
    <div class="shadow"></div>
    <!-- end top shadow -->

    <!-- The splash screen -->
    <div id="splash">
        <div class="loader">
            <img class="splash-logo" src="{{ asset('images/logo.png') }}" />
            <div class="line"></div>
        </div>
    </div>
    <!-- End of splash screen -->

    <div id="wrapper">
        <!-- main content -->
        <main>
            <header class="detail">
                <a href="{{ url('/') }}" class="back" data-transition="slide-from-top">
                    <h1>back</h1>
                </a>
                <section>
                    <h3 class="badge">قائمه الاطباق</h3>
                    <h1>اطباقنا المميزه</h1>
                </section>
            </header>
            <!-- end header -->
            <div class="content-wrap">
                <div class="content">
                    <i class="icon bg icon-Food"></i>
                    <section>
                        <header class="with-subnav">

                                <ul>

                                    @foreach ($categories as $category)

                                        <li data-attr="{{ $category->id }}" class="btn category-btn"><a><h4>{{ $category->name }}</h4></a></li>
                                        @endforeach

                                    </ul>

                        </header>

                        @foreach ($categories as $category)

            0<div class="taps"  id="{{ $category->id }}">
                        <ul class="price-list">
                            @foreach ($category->food as $food)
                            <li>
                                    <div class="top">
                                        <h3 class="title">{{ $food->name }}</h3>
                                        <h4 class="price badge">{{ $food->price }}</h4>
                                    </div>
                                    <p class="description">{{ $food->body }}</p>
                                </li>
                            @endforeach

                        </ul>
</div>
@endforeach

                    </section>
                </div>
            </div>

            <div data-remodal-id="modal">
                <i class="icon bg icon-CommentwithLines"></i>
                <button data-remodal-action="close" class="remodal-close"></button>
                <h1></h1>
                <p></p>
                <div class="signature center">
                    <h6>-CHEF-</h6>
                    <h5>Pierre Gabant</h5>
                </div>
            </div>
        </main>
        <!-- end of main content -->
    </div>

    <!-- The slideshow -->
    <ul id="slideshow" data-speed="6000">
        <li>
            <img src="https://ambiance.vagebond.nl/html/template/assets/img/slideshow/new/1.jpg" alt="slideshow image" />
        </li>
        <li>
            <img src="https://ambiance.vagebond.nl/html/template/assets/img/slideshow/new/2.jpg" alt="slideshow image" />
        </li>
        <li>
            <img src="https://ambiance.vagebond.nl/html/template/assets/img/slideshow/new/3.jpg" alt="slideshow image" />
        </li>
        <li>
            <img src="https://ambiance.vagebond.nl/html/template/assets/img/slideshow/new/4.jpg" alt="slideshow image" />
        </li>
        <li>
            <img src="https://ambiance.vagebond.nl/html/template/assets/img/slideshow/new/5.jpg" alt="slideshow image" />
        </li>
        <li>
            <img src="https://ambiance.vagebond.nl/html/template/assets/img/slideshow/new/6.jpg" alt="slideshow image" />
        </li>
        <li>
            <img src="https://ambiance.vagebond.nl/html/template/assets/img/slideshow/new/7.jpg" alt="slideshow image" />
        </li>
        <li>
            <img src="https://ambiance.vagebond.nl/html/template/assets/img/slideshow/new/8.jpg" alt="slideshow image" />
        </li>
        <li>
            <img src="https://ambiance.vagebond.nl/html/template/assets/img/slideshow/new/9.jpg" alt="slideshow image" />
        </li>
        <li>
            <img src="https://ambiance.vagebond.nl/html/template/assets/img/slideshow/new/10.jpg" alt="slideshow image" />
        </li>
        <li>
            <img src="https://ambiance.vagebond.nl/html/template/assets/img/slideshow/new/11.jpg" alt="slideshow image" />
        </li>
        <li>
            <img src="https://ambiance.vagebond.nl/html/template/assets/img/slideshow/new/12.jpg" alt="slideshow image" />
        </li>
        <li>
            <img src="https://ambiance.vagebond.nl/html/template/assets/img/slideshow/new/13.jpg" alt="slideshow image" />
        </li>
    </ul>
    <!-- end of slideshow -->

    <!-- Root element of PhotoSwipe. Must have class pswp. -->
    <div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">

        <!-- Background of PhotoSwipe.
         It's a separate element as animating opacity is faster than rgba(). -->
        <div class="pswp__bg"></div>

        <!-- Slides wrapper with overflow:hidden. -->
        <div class="pswp__scroll-wrap">

            <!-- Container that holds slides.
            PhotoSwipe keeps only 3 of them in the DOM to save memory.
            Don't modify these 3 pswp__item elements, data is added later on. -->
            <div class="pswp__container">
                <div class="pswp__item"></div>
                <div class="pswp__item"></div>
                <div class="pswp__item"></div>
            </div>


        </div>

    </div>
    <!-- scripts -->
    <script type="text/javascript" src="https://ambiance.vagebond.nl/html/template/assets/js/pack.js"></script>
    <!-- end of scripts -->

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"> </script>

        <script>
        $('.category-btn').first().addClass('active');

        $('.taps').first().addClass('active-pan');

        </script>

        <script>

                $('.category-btn').on('click', function () {

                    $(this).addClass('active').siblings().removeClass('active');

                    $('.taps').removeClass('active-pan');

                    var tapscont = $(this).attr('data-attr');

                   $('#' + tapscont).addClass('active-pan');
                });

        </script>
</body>

</html>
