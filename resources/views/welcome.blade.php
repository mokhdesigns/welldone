<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>{{ config('app.name')}}</title>

    <!-- mobile setup -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <link rel="stylesheet" href="{{ asset('website/css/style-cocktail.css')}}">
    <meta name="description" content="">
    <meta property="og:description" content="" />
    <link rel="icon" type="image/png" href="{{ asset('images/logo.png') }}">
    <link href="https://fonts.googleapis.com/css?family=Cairo|Reem+Kufi&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css">

</head>

<body>

    <!-- Top shadow -->
    <div class="shadow"></div>
    <!-- end top shadow -->

    <!-- The splash screen -->
    <div id="splash">
        <div class="loader">
            <img class="splash-logo" src="{{ asset('images/logo.png') }}" />
            <div class="line"></div>
        </div>
    </div>
    <!-- End of splash screen -->

    <div id="wrapper">
        <!-- main content -->
        <main>
            <header>
                <a href="" class="logo">
                    <h1>Cocktail</h1>
                    <img src="{{ asset('images/logo.png') }}" alt="Cocktail" />
                </a>
            </header>
            <!-- The navigation -->
            <nav class="strokes">
                <ul id="navigation">
                    <li>
                        <a href="{{ route('about') }}" data-transition="slide-to-top">
                            <section>
                                <h1>من نحن</h1>
                                <h5 class="badge-rounded">بعض المعلومات عنا  </h5>
                            </section>
                            <footer>
                                    <i class="far fa-address-card"></i>
                                <h5 class="serif">قصتنا</h5>
                                <p>عرض المزيد </p>
                            </footer>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('menu') }}" data-transition="slide-to-top">
                            <section>
                                <h1>قائمه الطعام</h1>
                                <h5 class="badge-rounded">ما يميزنا</h5>
                            </section>
                            <footer>
                                    <i class="fas fa-utensils"></i>
                                <h5 class="serif">مطبخنا</h5>
                                <p>عرض المزيد</p>
                            </footer>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('blog') }}" data-transition="slide-to-top">
                            <section>
                                <h1>أخبار</h1>
                                <h5 class="badge-rounded">اخبارنا</h5>
                            </section>
                            <footer>
                                    <i class="far fa-newspaper"></i>
                                <h5 class="serif">ما وراء الكواليس</h5>
                                <p>عرض المزيد</p>
                            </footer>
                        </a>
                    </li>

                    <li>
                        <a href="{{ route('contact') }}" data-transition="slide-to-top">
                            <section>
                                <h1>اتصل بنا</h1>
                                <h5 class="badge-rounded">اخبرنا عنك</h5>
                            </section>
                            <footer>
                                    <i class="fas fa-file-signature"></i>
                                <h5 class="serif">اترك لنا</h5>
                                <p>رساله</p>
                            </footer>
                        </a>
                    </li>
                </ul>
            </nav>
            <!-- End navigation -->
            <div class="overlay"></div>
            <div data-remodal-id="modal">
                <i class="icon bg icon-CommentwithLines"></i>
                <button data-remodal-action="close" class="remodal-close"></button>
                <h1></h1>
                <p></p>
            </div>
        </main>
        <!-- end of main content -->
    </div>

    <!-- The slideshow -->
    <ul id="slideshow" data-speed="6000">
        <li>
            <img src="{{ asset('images/1.jpg') }}" alt="slideshow image" />
        </li>
        <li>
            <img src="{{ asset('images/1.jpg') }}" alt="slideshow image" />
        </li>
        <li>
            <img src="{{ asset('images/1.jpg') }}" alt="slideshow image" />
        </li>
        <li>
            <img src="{{ asset('images/1.jpg') }}" alt="slideshow image" />
        </li>
        <li>
            <img src="{{ asset('images/1.jpg') }}" alt="slideshow image" />
        </li>
        <li>
            <img src="{{ asset('images/1.jpg') }}" alt="slideshow image" />
        </li>
        <li>
            <img src="{{ asset('images/1.jpg') }}" alt="slideshow image" />
        </li>
        <li>
            <img src="{{ asset('images/1.jpg') }}" alt="slideshow image" />
        </li>
        <li>
            <img src="{{ asset('images/1.jpg') }}" alt="slideshow image" />
        </li>
    </ul>
    <!-- end of slideshow -->

    <!-- Root element of PhotoSwipe. Must have class pswp. -->
    <div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">

        <!-- Background of PhotoSwipe.
         It's a separate element as animating opacity is faster than rgba(). -->
        <div class="pswp__bg"></div>

        <!-- Slides wrapper with overflow:hidden. -->
        <div class="pswp__scroll-wrap">

            <!-- Container that holds slides.
            PhotoSwipe keeps only 3 of them in the DOM to save memory.
            Don't modify these 3 pswp__item elements, data is added later on. -->
            <div class="pswp__container">
                <div class="pswp__item"></div>
                <div class="pswp__item"></div>
                <div class="pswp__item"></div>
            </div>

            <!-- Default (PhotoSwipeUI_Default) interface on top of sliding area. Can be changed. -->
            <div class="pswp__ui pswp__ui--hidden">

                <div class="pswp__top-bar">

                    <!--  Controls are self-explanatory. Order can be changed. -->

                    <div class="pswp__counter"></div>

                    <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>

                    <button class="pswp__button pswp__button--share" title="Share"></button>

                    <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>

                    <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>

                    <div class="pswp__preloader">
                        <div class="pswp__preloader__icn">
                            <div class="pswp__preloader__cut">
                                <div class="pswp__preloader__donut"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                    <div class="pswp__share-tooltip"></div>
                </div>

                <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
            </button>

                <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
            </button>

                <div class="pswp__caption">
                    <div class="pswp__caption__center"></div>
                </div>

            </div>

        </div>

    </div>
    <!-- scripts -->
    <script type="text/javascript" src="{{ asset('website/js/pack.js') }}"></script>
    <!-- end of scripts -->

    <script type="application/javascript">
        $(document).ready(function() {
            var currentUrl = window.location.href;

            $('body').on('pageActivated', function() {
                $('.back', '#wrapper').attr('href', currentUrl);
            });
        });
    </script>

</body>

</html>
