<?php

Route::prefix('dashboard')->name('dashboard.')->middleware(['auth','Admin'])->group(function () {

    Route::get('/', 'HomeController@index');

    Route::resource('about', 'AboutController');

    Route::resource('category', 'CategoryController');

    Route::resource('food', 'FoodController');

});
