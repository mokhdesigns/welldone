<?php


Route::get('/', function () {
    return view('welcome');
});

Route::get('about', "HomeController@about")->name('about');

Route::get('menu', "HomeController@menu")->name('menu');

Route::get('blog', "HomeController@blog")->name('blog');

Route::get('contact', "HomeController@contact")->name('contact');

Route::get('about', "HomeController@about")->name('about');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
